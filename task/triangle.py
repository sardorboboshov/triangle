def is_triangle(a, b, c):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''
    arr = [a,b,c]
    arr.sort()
    if arr[0] <= 0:
        return False
    if arr[2] >= arr[0] + arr[1]:
        return False
    return True
